package id.salissofar.jentikandroidgit;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class dbhelper extends RecyclerView.Adapter <dbhelper.dbvh> {
    private ArrayList<db> dblist ;

    public dbhelper(ArrayList<db> dblist) {
        this.dblist = dblist;
    }

    @NonNull
    @Override
    public dbvh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate (R.layout.itemjentik, parent, false);
        return new dbvh(v);
    }

    @Override
    public void onBindViewHolder(@NonNull dbvh holder, int position) {
        holder.tvnamaval.setText(dblist.get(position).getnama());
        holder.tvalamatval.setText(dblist.get(position).getalamat());
        holder.tvketeranganval.setText(dblist.get(position).getketerangan());
    }

    @Override
    public int getItemCount() {
        return dblist.size();
    }

    public class dbvh extends RecyclerView.ViewHolder {
        private TextView tvnamaval, tvalamatval, tvketeranganval;
        public dbvh(@NonNull View itemView) {
            super(itemView);
            tvnamaval =itemView.findViewById(R.id.tvnamaval);
            tvalamatval =itemView.findViewById(R.id.tvalamatval);
            tvketeranganval =itemView.findViewById(R.id.tvketeranganval);
        }
    }
}
