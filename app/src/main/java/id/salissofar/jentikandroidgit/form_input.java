package id.salissofar.jentikandroidgit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class form_input extends AppCompatActivity {

    TextView tampilUser;
    String myUser="username";
    String haloUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_input);
        tampilUser = findViewById(R.id.tvUsername);

        Bundle bundle_extra = getIntent().getExtras();
        haloUser = bundle_extra.getString(myUser);

        tampilUser.setText(haloUser);


        ArrayList<db> dblist;



                EditText ednama = (EditText) findViewById(R.id.ednama);
                EditText edalamat = (EditText) findViewById(R.id.edalamat);
                EditText edketerangan = (EditText) findViewById(R.id.edketerangan);
                Button btnsimpan = (Button) findViewById(R.id.btnsimpan);

                dblist = new ArrayList<>();

                Intent intent_list = new Intent(form_input.this, list.class);

                btnsimpan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String isian_nama = ednama.getText().toString();
                        String isian_alamat = edalamat.getText().toString();
                        String isian_keterangan = edketerangan.getText().toString();

                        if (isian_nama.isEmpty() || isian_alamat.isEmpty() || isian_keterangan.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Isian nama Masih kosong", Toast.LENGTH_SHORT).show();
                        } else {


                            dblist.add(new db(isian_nama, isian_alamat, isian_keterangan));
                            ednama.setText("");
                            edalamat.setText("");
                            edketerangan.setText("");
                            intent_list.putParcelableArrayListExtra("dblist", dblist);
                            startActivity(intent_list);
                        }
                    }

                });
                Button btnlihat = (Button) findViewById(R.id.btnlihat);
                btnlihat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(dblist.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Isian nama Masih kosong", Toast.LENGTH_SHORT).show();
                        }else{

                            intent_list.putParcelableArrayListExtra("dblist", dblist);

                            startActivity(intent_list);

                        }


                    }
                });
            }
        }












