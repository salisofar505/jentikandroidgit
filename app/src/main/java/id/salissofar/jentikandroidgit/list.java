package id.salissofar.jentikandroidgit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class list extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        RecyclerView recyclerView= (RecyclerView) findViewById(R.id.list);


        ArrayList<db> dblist = getIntent().getExtras().getParcelableArrayList("dblist");
        dbhelper dbhelper = new dbhelper(dblist);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(list.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(dbhelper);
    }
}