package id.salissofar.jentikandroidgit;

import android.os.Parcel;
import android.os.Parcelable;

public class db implements Parcelable {

    String nama ;
    String alamat;
    String keterangan;

    public db(String nama, String alamat, String keterangan) {
        this.nama = nama;
        this.alamat = alamat;
        this.keterangan = keterangan;
    }

    public String getnama() {
        return nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getalamat() {
        return alamat;
    }

    public void setalamat(String alamat) {
        this.alamat = alamat;
    }

    public String getketerangan() {
        return keterangan;
    }

    public void setketerangan(String keterangan) {
        this.keterangan = keterangan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama);
        dest.writeString(this.alamat);
        dest.writeString(this.keterangan);
    }

    public void readFromParcel(Parcel source) {
        this.nama = source.readString();
        this.alamat = source.readString();
        this.keterangan = source.readString();
    }

    protected db(Parcel in) {
        this.nama = in.readString();
        this.alamat = in.readString();
        this.keterangan = in.readString();
    }

    public static final Creator<db> CREATOR = new Creator<db>() {
        @Override
        public db createFromParcel(Parcel source) {
            return new db(source);
        }

        @Override
        public db[] newArray(int size) {
            return new db[size];
        }
    };
}
